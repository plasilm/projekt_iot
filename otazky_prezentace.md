# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala|7 - 8 hodin |
| jak se mi to podařilo rozplánovat|Měl jsem velké oči... Ale jinak jsem spokojený |
| návrh designu | Klasická křižovatka |
| proč jsem zvolil tento design|Je jednoduchý a líbivý... Baráky jsou pěkné... Semafory už tolik ne :D |
| zapojení | *URL obrázku zapojení* |
| z jakých součástí se zapojení skládá |Součástky z Láska-kitu... Drátky, rezistory, fotorezistor, ledky |
| realizace | *URL obrázku hotového produktu* |
| jaký materiál jsem použil a proč |papír, pájku, lepidlo... |
| co se mi povedlo |design |
| co se mi nepovedlo/příště bych udělal/a jinak |semafory |
| zhodnocení celé tvorby |nebavilo mě to, popálil jsem se a trochu se přiotrávil... 10/10 |